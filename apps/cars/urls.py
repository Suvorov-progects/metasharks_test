from django.urls import path
from rest_framework.routers import SimpleRouter
from apps.cars import views


router = SimpleRouter()

router.register('api/colors', views.CarColorViewSet, 'colors')
router.register('api/models', views.CarModelViewSet, 'models')
router.register('api/brands', views.CarBrandViewSet, 'brands')
router.register('api/orders', views.CarOrderViewSet, 'orders')


urlpatterns = [
    path('api/orders-count/', views.CarAttributesCountAPIView.as_view()), *router.urls, 
    path('api/crete_rake',views.CreateFakeData.as_view()), *router.urls,           
]
