## СТЕК:
    * linux
    * python 3.10
    * django 4.1
    * Postgres 13
    * drf
    * silk
    * swagger
    * djangorestframework-simplejwt

## Запуск

1. Создать .env-файл в корне проекта со следующими полями
* POSTGRES_NAME=mesh
* POSTGRES_USER=mesh
* POSTGRES_PASSWORD=mesh
* POSTGRES_HOST=localhost
* POSTGRES_PORT=5432
* PROJECT_URL=http://0.0.0.0:8000
* JWT_ACCESS_TOKEN_LIFETIME=12
* JWT_REFRESH_TOKEN_LIFETIME=1

2. Создать БД:
```angular2html
sudo -u postgres psql
CREATE DATABASE mesh;
```
создать пользователя с правами:
```angular2html
CREATE USER mesh WITH password 'mesh';
GRANT ALL ON DATABASE mesh TO mesh;
```
3. Клонировать репозиторий, командой ```git clone https://gitlab.com/Suvorov-progects/metasharks_test/-/tree/master``` после чего активировать виртуальное окружение 
4. Установить все необходимые зависимости командой ```pip install -r requirements.txt```
5. Перейти в клонированную директорию коммандой ```cd backend```
6. Провести миграцию командой ```python manage.py makemigrations```
7. Применить миграцию командой ```python manage.py migrate```
8. Запустите приложение командой ```python manage.py runserver```
9. Сервер запущен по адресу ```http://localhost:8000/``
10. Создать суперпользователя командой ```python manage.py createduperuser``` введите имя ```admin``` и пароль ```admin```
11. адреса для запросов:
```
Для того чтобы получить доступ к эндпоинтам нужно:

- Заполнить БД фейковыми данными
- Пройти аутентификацию и получить токен
```

* сваггер по эндпоинтам доступен по пути http://127.0.0.1:8000/swagger/
* генерация БД - перейти по адресу http://127.0.0.1:8000/api/crete_rake

* 'api/colors'
* 'api/models'
* 'api/brands'
* 'api/orders'
* 'api/orders-count/'
