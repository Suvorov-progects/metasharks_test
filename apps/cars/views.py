from django_filters import filterset
from django.db.models import Count
from django.db import transaction
from drf_yasg.utils import swagger_auto_schema, no_body
from rest_framework import viewsets, views
from rest_framework.response import Response
from faker import Faker
import random

from apps.cars.models import Color, Brand, Model, Order
from apps.cars.serializers import CarColorSerializer, CarModelSerializer
from apps.cars.serializers import CarBrandSerializer, CarOrderSerializer


class CreateFakeData(views.APIView):
    """
    Создание записей в БД
    """
    queryset = Color.objects.all()
    
    def get(self, request, *args, **kwargs):    # queryset = User.objects.all()
        count = 100
        fake = Faker('ru_RU')
        with transaction.atomic():
            # записи Color
            val_calor = []
            code_name = ['white', 'red', 'blue', 'yellow', 'pink', 'orange', 'grey']
            h_color = ['#82a8f4', '#fbbaf7', '#e8e1e9', '#a45a52', '#c8ad7f', '#16645f', '#9999ff']
            val = random.randrange(len(h_color))
            
            for _ in range(0, count):
                fields = {
                    'code_name': random.choice(code_name),
                    'hex_value':random.choice(h_color),
                }
                val_calor.append(Color(**fields))
            Color.objects.bulk_create(val_calor, batch_size=1000)
            
             # записи Brand
            val_brand = []
            for _ in range(0, count):
                fields = {
                    'code_name': f'{fake.words()}',
                }
                val_brand.append(Brand(**fields))            
            Brand.objects.bulk_create(val_brand, batch_size=1000)
            
            # записи Model
            val_model = []
            for _ in range(0, count):
                fields = {
                    'code_name': f'{fake.words()}',
                }
                val_model.append(Model(**fields))
            Model.objects.bulk_create(val_model, batch_size=1000)
        
        with transaction.atomic():
            # записи Order
            colors = Color.objects.all()
            brands = Brand.objects.all()
            models = Model.objects.all()
            
            val_order = []
            for _ in range(0, count):
                c_s = random.choice(colors)
                b_s = random.choice(brands)
                m_s = random.choice(models)
                fields = {
                    'color': c_s,
                    'brand': b_s,
                    'model': m_s,
                    'amount': random.randint(1, 100),
                }
                val_order.append(Order(**fields))
            Order.objects.bulk_create(val_order, batch_size=1000)
            
        return Response({'result': 'создано 100 записей в БД'})
    
    
class OrderFilterSet(filterset.FilterSet):
    color = filterset.CharFilter('color__code_name')
    model = filterset.CharFilter('model__code_name')
    brand = filterset.CharFilter('brand__code_name')


class CarColorViewSet(viewsets.ModelViewSet):
    """ CRUD для работы с цветами авто """
    queryset = Color.objects.all()
    serializer_class = CarColorSerializer


class CarModelViewSet(viewsets.ModelViewSet):
    """ CRUD для работы с моделями авто """
    queryset = Model.objects.all()
    serializer_class = CarModelSerializer


class CarBrandViewSet(viewsets.ModelViewSet):
    """ CRUD для работы с марками авто """
    queryset = Brand.objects.all()
    serializer_class = CarBrandSerializer


class CarOrderViewSet(viewsets.ModelViewSet):
    """ CRUD для работы с заказами """
    filterset_class = OrderFilterSet
    serializer_class = CarOrderSerializer
    queryset = Order.objects.select_related('color', 'model', 'brand')


class CarAttributesCountAPIView(views.APIView):
    """ API для вывода список цветов, моделей и марок """

    def get(self, request, *args, **kwargs):
        color = Color.objects.annotate(orders=Count('order')).values('code_name', 'orders')
        model = Model.objects.annotate(orders=Count('order')).values('code_name', 'orders')
        brand = Brand.objects.annotate(orders=Count('order')).values('code_name', 'orders')

        return Response({
            'colors': color,
            'models': model,
            'brands': brand
        })
